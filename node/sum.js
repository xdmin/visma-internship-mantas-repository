const list = [1, 2, 3, 3, 1, 23, 4];
//ES 6
let reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(list.reduce(reducer));

//ES 5
function total() {
    var sum = 0;
    for (var i = 0; i < list.length; i++) {
        sum += list[i];
    }
    console.log(sum)
}

total();