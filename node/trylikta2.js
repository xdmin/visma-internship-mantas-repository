function spy() {
    let count = 0;

    function wrapper() {
        count++;
        return myFunction();
    }


    function report() {
        console.log(count);
    }
    spy.wrapper = wrapper.bind(this);
    spy.report = report.bind(this);
    return spy;
}

var spy = spy();

spy.wrapper();
spy.wrapper();
spy.wrapper();
spy.report();









var counter = 0;

function myFunction(num) {
    console.log('Passed number: ' + num);
}

var spy = function(func) {

    function call(num) {
        counter++;
        return func(num);
    }

    function report() {
        return { totalCalls: counter };
    }

    call.report = report;
    return call;
}

var spied = spy(myFunction);

spied(1);
spied(2);
console.log(spied.report());