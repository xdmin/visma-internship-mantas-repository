class Spy {
    constructor() {
        this.count = 0;
    }

    wrapper() {
        this.count++;
        return this.myFunction();
    }

    myFunction() {
        console.log('trigger');
    }

    report() {
        console.log(this.count);
    }
}

let spy = new Spy();
var asd = spy.wrapper();
var asd = spy.wrapper();
var asd = spy.wrapper();

var report = spy.report();
