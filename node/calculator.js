class Calculator {
    constructor() {
        this.total = 0;
    }

    add(amount) {
        this.total += amount;
        return this
    }

    subtract(amount) {
        this.total -= amount;
        return this
    }

    mutiply(amount) {
        this.total *= amount;
        return this
    }

    divide(amount) {
        this.total /= amount;
        return this
    }
}

calc = new Calculator();

console.log(calc.add(12).subtract(6).mutiply(3).divide(4));

function CalculatorOld() {
    this.totalOld = 0;
}

CalculatorOld.prototype.addOld = function (amount) {
    this.totalOld += amount;
    return this
};

CalculatorOld.prototype.subtractOld = function (amount) {
    this.totalOld -= amount;
    return this
};

CalculatorOld.prototype.multiplyOld = function (amount) {
    this.totalOld *= amount;
    return this
};

CalculatorOld.prototype.divideOld = function (amount) {
    this.totalOld /= amount;
    return this
};

var oldCalc = new CalculatorOld();
console.log(oldCalc.addOld(12).subtractOld(6).multiplyOld(3).divideOld(4));
