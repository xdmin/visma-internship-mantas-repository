sevenAte9('79712312'); // returns '7712312'
sevenAte9('799797'); // returns '777'

function sevenAte9(array) {
    const replacedArray = array.replace('797', '77');

    if (replacedArray !== array) {
        sevenAte9(replacedArray);
    } else {
        console.log(replacedArray);
    }
}
