const colors = require('colors');
colorsList = [colors.red, colors.green, colors.yellow, colors.blue];

let color;

function selectColor() {

    let lastColor = color;
    let selector = Math.random() * 2.5;
    if (selector < 0.25) {
        color = 0;
    }
    if (selector > 0.25 && selector < 0.5) {
        color = 1;
    }
    if (selector > 0.5 && selector < 0.75) {
        color = 2;
    }
    if (selector > 0.75) {
        color = 3;
    }
    if (lastColor === color) {
        selectColor();
    }
}

let i = 0;
let triangle = '#';
do {
    selectColor();
    console.log(colorsList[color](triangle));
    triangle += '#';
    i++

} while (i < 7);