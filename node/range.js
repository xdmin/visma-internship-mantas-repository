function rangeES5(x, y) {
    var numbersArray = [];

    for (var i = x; i < y; i++) {
        numbersArray.push(i);
    }
    console.log('ECMA 5: ' + numbersArray);
}

rangeES5(4, 9);


function rangeES6(x, y) {
    console.log('ECMA 6: ' + Array.from(new Array(y - x), (anything, i) => x + i));
}

rangeES6(4, 9);