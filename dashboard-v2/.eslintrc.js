module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 2015,
    'sourceType': 'module'
  },
  'rules': {
    'max-len': [
      'error',
      120
    ],
    'template-curly-spacing': [
      'error',
      'always'
    ],
    'prefer-const': [
      'error'
    ],
    'indent': [
      'error',
      'tab'
    ],
    'linebreak-style': [
      'error',
      'windows'
    ],
    'no-useless-return': [
      'error'
    ],
    'no-multiple-empty-lines': [
      'error'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'no-console': [
      'error', {
        'allow': [
          'warn',
          'error'
        ]
      }
    ],
    'no-const-assign': [
      'error'
    ],
    'no-extra-semi': [
      'error'
    ],
    'no-irregular-whitespace': [
      'error'
    ],
    'no-mixed-spaces-and-tabs': [
      'error'
    ],
    'no-undef': [
      'error'
    ],
    'no-unexpected-multiline': [
      'error'
    ],
    'no-unreachable': [
      'error'
    ],
    'no-unused-vars': [
      'error'
    ],
    'arrow-spacing': [
      'error', {
        'before': true,
        'after': true
      }
    ],
    'curly': [
      'error'
    ],
    'no-multi-spaces': [
      'error'
    ],
    'no-trailing-spaces': [
      'error'
    ],
    'no-undefined': [
      'error'
    ],
    'no-unneeded-ternary': [
      'error'
    ],
    'no-unused-expressions': [
      'error'
    ],
    'no-useless-constructor': [
      'error'
    ],
    'no-var': [
      'error'
    ],
    'object-curly-newline': [
      'error'
    ],
    'object-property-newline': [
      'error'
    ],
    'dot-location': [
      'error',
      'property'
    ],
    'no-magic-numbers': [
      'error', {
        'ignoreArrayIndexes': true,
        'ignore': []
      }
    ]
  }
}