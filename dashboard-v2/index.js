import {RestService} from './services/restService/restService.js';
import * as Widgets from './components/widgetsList/widgets.js';
import * as Constants from './constants/constants.js';
import {showHide} from './helperFunctions/helperFunctions.js';

const app = function () {
	document.getElementById('goBack').onclick = showHide;
	const widgetsContainer = document.getElementById('widgetsContainer');
	const restService = new RestService();
	restService.getData(Constants.backEndUrl, '/widgets')
		.then(allWidgets => {
			const allWidgetsStored = allWidgets;
			Widgets.setWidgetsColumn(allWidgetsStored, widgetsContainer);
			Widgets.addNewWidget(widgetsContainer);
		});
};
app();

//TODO restService logic to widgetsList, functional programming - immutable data
//TODO navbar, sidebar css to styles folder not components