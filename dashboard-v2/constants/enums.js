export const WidgetTypeEnum = {
    'chat': 1,
    'usersList': 2,
    'smallList': 3
};
export const HeaderTypeEnum = {
    'header-dark': 1,
    'header-light': 2,
};

export const ColumnEnum = {
    'column--1': 1,
    'column--2': 2,
    'column--3': 3
};

Object.freeze(WidgetTypeEnum);
Object.freeze(HeaderTypeEnum);
