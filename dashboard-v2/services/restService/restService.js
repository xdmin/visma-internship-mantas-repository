export class RestService {

    getData(url, urlExtension) {
        return fetch(url + urlExtension)
            .then(response => response.json()
            ).catch(err => {
                console.log(err)
            });
    }

    postData(url, urlExtension, data) {
        fetch(url + urlExtension, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .catch(error => console.error(`Fetch Error =\n`, error));
    }

    putData(url, urlExtension, data) {
        fetch(url + urlExtension, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .catch(error => console.error(`Fetch Error =\n`, error));
    }

    deleteData(url, urlExtension) {
        fetch(url + urlExtension, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        }).then(response => response.json())
            .catch(error => console.error(`Fetch Error =\n`, error));
    }
}

