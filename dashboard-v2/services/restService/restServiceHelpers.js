import {RestService} from "../../services/restService/restService.js";
import * as Constants from "../../constants/constants.js";

const restService = new RestService();

export function getData(id) {
    return restService.getData(Constants.backEndUrl, id)
}

export function postData(data, url) {
    return restService.postData(Constants.backEndUrl, url, data)
}

export function putData(data, id) {
    return restService.putData(Constants.backEndUrl, id, data)
}

export function deleteData(id) {
    return restService.deleteData(Constants.backEndUrl, id)
}