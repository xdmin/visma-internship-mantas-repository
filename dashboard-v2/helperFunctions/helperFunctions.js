import * as Enums from '../constants/enums.js';
import {WidgetTypeEnum} from '../constants/enums.js';
import * as DummyData from '../dummyData/dummyData.js';
import {form} from '../constants/constants.js';

export function convertHeaderTypeToClassName (headerType) {
	switch (headerType) {
		case Enums.HeaderTypeEnum['header-dark']:
			return 'header-dark';

		case Enums.HeaderTypeEnum['header-light']:
			return 'header-light';
	}
}

export function convertColumnTypeToClassName (column) {
	switch (column) {
		case Enums.ColumnEnum['column--1']:
			return 'column--1';
		case Enums.ColumnEnum['column--2']:
			return 'column--2';
		case Enums.ColumnEnum['column--3']:
			return 'column--3';
	}
}

export function fillFormTemplate (data, isChat) {
	document.getElementById('name').innerText = ' ' + data.title;
	document.getElementById('title').value = data.title;
	document.getElementById('column').value = data.column;
	document.getElementById('widgetType').value = data.type;
	document.getElementById('headerType').value = data.headerType;
	document.getElementById('messagesInput').value = JSON.stringify(data.data);
	if (isChat) {
		document.getElementById('usersGroupInput').value = JSON.stringify(data.users);
	}
}

export function showHideDefaultUsersList () {
	const selectedValue = document.getElementById('widgetType').value;
	const defaultUsersListTitle = document.getElementById('defaultUsersListTitle');
	const usersGroupInput = document.getElementById('usersGroupInput');
	if (parseInt(selectedValue) === WidgetTypeEnum.chat) {
		defaultUsersListTitle.className = '';
		usersGroupInput.className = '';
		document.getElementById('usersGroupInput').value = DummyData.defaultUsersList;
		return;
	}
	defaultUsersListTitle.className = 'hide';
	usersGroupInput.className = 'hide';
//TODO toggle class names // or add (append)/remove class name
}

export function showHide () {
	document.getElementById('widgetsContainer')
		.classList
		.toggle('blur');
	document.getElementById('editContainer')
		.classList
		.toggle('hide');
}

export function fillForm (data) {
	const defaultUsersListTitle = document.getElementById('defaultUsersListTitle');
	const usersGroupInput = document.getElementById('usersGroupInput');
	if (data.type === WidgetTypeEnum.chat) {
		defaultUsersListTitle.className = '';
		usersGroupInput.className = '';
		fillFormTemplate(data, true);
		return;
	}
	defaultUsersListTitle.className = 'hide';
	usersGroupInput.className = 'hide';
	fillFormTemplate(data);
}

//TODO replace multiple document.getElementById to container and get its children?
//TODO BUG detected: in 2 row screen width element with row-1 going after row-3 has wrong place.
//Todo BUG solution: js resolution check n edit max-columns.
//TODO create separate files for helper functions and naming  (shared folder)