import * as Templates from "./usersListWidgetTemplates.js";

export class WidgetUsersList {
    constructor(widgetsContainer, widgetsData) {
        this.widgetsContainer = widgetsContainer;
        this.widgetsData = widgetsData;
        this.usersList = '';

        this.generateWidget();
    }

    generateUsersList() {
        this.widgetsData.data.forEach((item) => {
            this.usersList += Templates.usersListGenerator(item);
        });
    }

    generateWidget() {
        const usersListContainer = Templates.usersListContainerTemplate(this.widgetsData);

        this.generateUsersList();
        usersListContainer.appendChild(Templates.usersListHeaderTemplate(this.widgetsData));
        usersListContainer.appendChild(Templates.usersListContentTemplate(this.widgetsData, this.usersList));
        this.widgetsContainer.appendChild(usersListContainer);
    }
}
