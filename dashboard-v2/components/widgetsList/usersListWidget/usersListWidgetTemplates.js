import * as HelperFunctions from '../../../helperFunctions/helperFunctions.js';
import {form} from '../../../constants/constants.js';

export function usersListContainerTemplate (widgetsData) {
	const usersListContainer = document.createElement('div');
	usersListContainer.className = `widget-large-users-list shadow ${ HelperFunctions.convertColumnTypeToClassName(widgetsData.column) }`;
	return usersListContainer;
}

export function usersListContentTemplate (widgetsData, usersList) {
	const usersListContent = document.createElement('div');
	usersListContent.className = 'widget-large-users-list-content';
	usersListContent.innerHTML = usersList;
	return usersListContent;
}

export function usersListHeaderTemplate (widgetsData) {
	const usersListHeader = document.createElement('div');
	usersListHeader.onclick = () => form.editForm(widgetsData);
	usersListHeader.className = `widget-users-list ${ HelperFunctions.convertHeaderTypeToClassName(widgetsData.headerType) }`;
	usersListHeader.innerHTML = `
                    <span>
                        <div class="top-left-radius5 height-1em"></div>
                        <div>#</div>
                    </span>
                    <span>
                        <div>First</div>
                        <div>Name</div>
                    </span>
                    <span>
                        <div>Last</div>
                        <div>Name</div>
                    </span>
                    <span>
                        <div class="height-1em"></div>
                        <div class="top-right-radius5">Username</div>
                    </span>`;
	return usersListHeader;
}

export function usersListGenerator (item) {
	return `<span>${ item.id }</span>
            <span>${ item.firstName }</span>
            <span>${ item.lastName }</span>
            <span>${ item.username }</span>`;
}