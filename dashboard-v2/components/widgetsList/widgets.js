import {WidgetChat} from './chatWidget/chatWidget.js';
import {WidgetTypeEnum} from '../../constants/enums.js';
import {WidgetUsersList} from './usersListWidget/usersListWidget.js';
import {form} from '../../constants/constants.js';
import * as Constants from '../../constants/constants.js';
import * as DummyData from '../../dummyData/dummyData.js';
import * as RestHelper from '../../services/restService/restServiceHelpers.js';

const maxColumns = 3;
let currentColumn = 1;

export function changeCurrentColumn () {
	currentColumn++;
	if (currentColumn > maxColumns) {
		currentColumn = 1;
	}
}

export function setWidgetsColumn (allWidgetsStored, widgetsContainer) {
//TODO try find.
	for (let i = 0; i < allWidgetsStored.length; i++) {
		if (allWidgetsStored[i].column === currentColumn) {
			addWidget(allWidgetsStored[i], widgetsContainer);
			allWidgetsStored.splice(i, 1);
			break;
		}
	}

	if (allWidgetsStored.length) {
		changeCurrentColumn();
		setWidgetsColumn(allWidgetsStored, widgetsContainer);
	}
}

export function addWidget (item, widgetsContainer) {
	if (item.type === WidgetTypeEnum.chat) {
		new WidgetChat(widgetsContainer, item);
	}
	if (item.type === WidgetTypeEnum.usersList) {
		new WidgetUsersList(widgetsContainer, item);
	}
}

export function addNewWidget (widgetsContainer) {
	const addWidgetSign = document.createElement('div');
	addWidgetSign.className = 'add-widget shadow';
	addWidgetSign.innerHTML = `<img src=\"imgs/plus-sign.svg\">`;
	addWidgetSign.onclick = () => form.createForm();
	widgetsContainer.appendChild(addWidgetSign);
}

export function updateWidget (widgetsData) {
	const data = {
		title: document.getElementById('title').value,
		column: +document.getElementById('column').value,
		type: parseInt(document.getElementById('widgetType').value),
		headerType: parseInt(document.getElementById('headerType').value),
		data: JSON.parse(document.getElementById('messagesInput').value),
		users: JSON.parse(DummyData.defaultUsersList)
	};
	RestHelper.putData(data, `/widgets/${widgetsData.id}`);
	window.location.replace(Constants.frondEndUrl);
}

export function deleteWidget (widgetsData) {
	RestHelper.deleteData(`/widgets/${widgetsData.id}`);
	window.location.replace(Constants.frondEndUrl);
}

export function createWidget () {
	const data = {
		title: document.getElementById('title').value,
		column: +document.getElementById('column').value,
		type: parseInt(document.getElementById('widgetType').value),
		headerType: parseInt(document.getElementById('headerType').value),
		data: JSON.parse(document.getElementById('messagesInput').value),
		users: JSON.parse(DummyData.defaultUsersList)
	};
	RestHelper.postData(data, '/widgets');
	window.location.replace(Constants.frondEndUrl);
}

//TODO wrapper function or class and save widgets on class property
//TODO throw away the grid and refactor sorting