import * as Templates from './chatWidgetTemplates.js';

export class WidgetChat {
	constructor (widgetsContainer, widgetsData) {
		this.widgetsContainer = widgetsContainer;
		this.widgetsData = widgetsData;
		this.messageContainer = '';

		this.generateWidget();
	}

	//TODO make init instead of constructor for init functions
	generateWidget () {
		const chatMainContainer = Templates.chatMainContainerTemplate(this.widgetsData);
		this.widgetsData.data.forEach((item) => {
			this.generateMessage(item);
		});
		chatMainContainer.appendChild(Templates.chatWidgetHeader(this.widgetsData));
		chatMainContainer.appendChild(Templates.chatContainerTemplate(this.widgetsData, this.messageContainer));
		this.widgetsContainer.appendChild(chatMainContainer);
	}

	generateMessage (item) {
		this.messageContainer += Templates.messageContainerTemplate(item, this.widgetsData);
	}
}
