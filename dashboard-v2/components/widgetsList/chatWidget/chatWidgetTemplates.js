import * as HelperFunctions from '../../../helperFunctions/helperFunctions.js';
import {form} from '../../../constants/constants.js';

export function chatMainContainerTemplate (widgetsData) {
	const chatMainContainer = document.createElement('div');
	chatMainContainer.className = `widget-chat shadow ${HelperFunctions.convertColumnTypeToClassName(widgetsData.column)}`;
	return chatMainContainer;
}

export function messageContainerTemplate (item, widgetsData) {
	return `<div class="message-container"><div class="contact-icon"></div><div class="contact-message">
            <div class="message-header">${widgetsData.users[item.senderId - 1].firstName}</div>
            <div class="message-data">${item.message}</div></div></div>`;
}

export function chatWidgetHeader (widgetsData) {
	const chatHeader = document.createElement('div');

	chatHeader.className = `sprite-after sprite-settings-icon ${HelperFunctions.convertHeaderTypeToClassName(widgetsData.headerType)}`;
	chatHeader.innerHTML = `Conversation: <span class="bold">${widgetsData.title}</span>           `;
	chatHeader.onclick = () => form.editForm(widgetsData);
	return chatHeader;
}

//TODO instead of get.document use query selector inside template(container)

export function chatContainerTemplate (widgetsData, messageContainer) {
	const chatContainer = document.createElement('div');

	chatContainer.innerHTML = `<hr>
            <div class="widget-chat-container">${messageContainer}</div>
            <textarea placeholder="Type and press enter"></textarea>
            <div class="send-button sprite-before">Send</div>`;

	return chatContainer;
}
