import * as Constants from "../../constants/constants.js";
import * as HelperFunctions from "../../helperFunctions/helperFunctions.js";
import * as RestHelper from "../../services/restService/restServiceHelpers.js";
import * as Widgets from "../../components/widgetsList/widgets.js";

export class CreateEditForm {
    constructor() {
        this.button = document.getElementById('button');
        this.buttonDelete = document.getElementById('buttonDelete');
        this.widgetType = document.getElementById('widgetType');
        this.goBack = document.getElementById('goBack');
        this.goBack.setAttribute('href', Constants.frondEndUrl);
        this.widgetType.onchange = HelperFunctions.showHideDefaultUsersList;
    }

    editForm(widgetsData) {
        HelperFunctions.showHide(); //toggle, with boolean
        this.button.onclick = () => Widgets.updateWidget(widgetsData);
        this.button.innerText = 'Submit';
        this.buttonDelete.className = 'button';
        this.buttonDelete.onclick = () => Widgets.deleteWidget(widgetsData);

        RestHelper.getData(`/widgets/${widgetsData.id}`).then((widgetData) =>
            HelperFunctions.fillForm(widgetData));
    }

    createForm() {
        HelperFunctions.showHide();
        const widgetFormName = document.getElementById('name');
        this.buttonDelete.className += ' hide';
        this.button.innerText = 'Create';
        this.button.onclick = () => Widgets.createWidget();
        widgetFormName.innerText = `You are creating new widget`;
        HelperFunctions.showHideDefaultUsersList();
    }
}

//TODO move CRUD here