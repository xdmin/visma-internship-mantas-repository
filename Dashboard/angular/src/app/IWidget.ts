export interface IWidget {
  id: number;
  name: string;
  column: number;
  headerType: number;
  title: string;
  type: number;
}

export interface IWidgetChat extends IWidget {
  users: Array<IWidgetChatUsersList>;
  data: Array<IWidgetChatMessage>
}

export interface IWidgetChatMessage {
  senderId: number;
  message: string;
}

export interface IWidgetChatUsersList {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  photo: URL;
}

export interface IWIdgetUserLists extends IWidget {
  data: Array<IWidgetUsersListUsersList>;
}

export interface IWidgetUsersListUsersList {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
}
