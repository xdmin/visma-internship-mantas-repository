import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  host: string;


  constructor(public api: HttpClient) {
    this.host = ' http://localhost:3000'
  }

  get(url) {
    return this.api.get(`${this.host}/${url}`)
  }

}
