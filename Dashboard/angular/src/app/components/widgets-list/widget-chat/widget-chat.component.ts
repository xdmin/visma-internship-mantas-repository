import {Component, Input, OnInit} from '@angular/core';
import {IWidgetChat} from "../../../IWidget";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CreateEditWidgetFormComponent} from "../../create-edit-widget-form/create-edit-widget-form.component";

@Component({
  selector: 'app-widget-chat',
  templateUrl: './widget-chat.component.html',
  styleUrls: ['./widget-chat.component.scss']
})
export class WidgetChatComponent implements OnInit {
  @Input() chatWidget: IWidgetChat;
  constructor() {
  }

  ngOnInit() {
    console.log(this.chatWidget)
  }
  // openVerticallyCentered(content) {
  //   this.modalService.open(content, {centered: true});
  // }


}
