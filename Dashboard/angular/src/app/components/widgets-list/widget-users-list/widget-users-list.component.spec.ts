import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetUsersListComponent } from './widget-users-list.component';

describe('WidgetUsersListComponent', () => {
  let component: WidgetUsersListComponent;
  let fixture: ComponentFixture<WidgetUsersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetUsersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
