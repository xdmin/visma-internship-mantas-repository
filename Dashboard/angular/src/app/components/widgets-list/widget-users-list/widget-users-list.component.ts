import {Component, Input, OnInit} from '@angular/core';
import {IWIdgetUserLists} from "../../../IWidget";

@Component({
  selector: 'app-widget-users-list',
  templateUrl: './widget-users-list.component.html',
  styleUrls: ['./widget-users-list.component.scss'],
})
export class WidgetUsersListComponent implements OnInit {
 @Input() usersList: IWIdgetUserLists;

  constructor() {
  }

  ngOnInit() {
    console.log(this.usersList)
  }

}
