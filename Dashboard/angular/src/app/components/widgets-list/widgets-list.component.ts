import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-widgets-list',
  templateUrl: './widgets-list.component.html',
  styleUrls: ['./widgets-list.component.scss']
})
export class WidgetsListComponent implements OnInit {
  url;
  host;
  columnOne: any[];
  columnTwo: any[];
  columnThree: any[];

  constructor(public api: ApiService) {
    this.url = 'widgets';
    this.host = 'http://localhost:3000/widgets';
    this.columnOne = [];
    this.columnTwo = [];
    this.columnThree = [];
  }

  ngOnInit() {
    this.getAllWidgets()
  }

  getAllWidgets(): any {
    this.api.get(this.url).subscribe((response: any) => {
      this.everyWidgetHasItsColumn(response)
    }, (err: any) => {
      console.log(err.message)
    })
  }

  everyWidgetHasItsColumn(allWidgets) {
    allWidgets.forEach((widget) => {
      if (widget.column === 1) {
        this.columnOne.push(widget)
      } else if (widget.column === 2) {
        this.columnTwo.push(widget)
      } else {
        this.columnThree.push(widget)
      }
    })
  }
}

// @Pipe({
//   name:"columnFilter"
// })
// class SomePipe {
//   transform(value:any[]){
//
//   }
// }
//
// @Pipe({name: 'eventPipe'})
// export class columnFilter implements PipeTransform {
//   transform(value: Array<IEvent>, day: number, month: number): IEvent[] {
//     return  value.filter(item => (+item.date.split('-')[2] === day) && (+item.date.split('-')[1] === month + 1));
//   }
// }
//
// getHeroes()
// :
// Observable < Hero[] > {
//   return this.http.get<Hero[]>(this.heroesUrl)
//     .pipe(
//       tap(heroes => this.log('fetched heroes')),
//       catchError(this.handleError('getHeroes', []))
//     );
// }
