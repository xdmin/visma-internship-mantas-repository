import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-edit-widget-form',
  templateUrl: './create-edit-widget-form.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./create-edit-widget-form.component.scss'],

})
export class CreateEditWidgetFormComponent implements OnInit {
  closeResult: string;

  constructor(public modalService: NgbModal) {
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, {centered: true});
  }

  ngOnInit() {
  }
}
