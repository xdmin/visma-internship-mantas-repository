import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditWidgetFormComponent } from './create-edit-widget-form.component';

describe('CreateEditWidgetFormComponent', () => {
  let component: CreateEditWidgetFormComponent;
  let fixture: ComponentFixture<CreateEditWidgetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditWidgetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditWidgetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
