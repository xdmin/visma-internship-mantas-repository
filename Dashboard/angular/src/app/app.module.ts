import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { WidgetsListComponent } from './components/widgets-list/widgets-list.component';
import { WidgetChatComponent } from './components/widgets-list/widget-chat/widget-chat.component';
import { WidgetUsersListComponent } from './components/widgets-list/widget-users-list/widget-users-list.component';
import { HttpClientModule} from "@angular/common/http";
import {ApiService} from "./services/api.service";
import {FormsModule} from "@angular/forms";
import { ContentContainerComponent } from './components/content-container/content-container.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './components/modal/modal.component';
import { CreateEditWidgetFormComponent } from './components/create-edit-widget-form/create-edit-widget-form.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    WidgetsListComponent,
    WidgetChatComponent,
    WidgetUsersListComponent,
    ContentContainerComponent,
    ModalComponent,
    CreateEditWidgetFormComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
